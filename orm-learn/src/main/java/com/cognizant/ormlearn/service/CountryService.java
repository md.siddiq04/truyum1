package com.cognizant.ormlearn.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cognizant.ormlearn.model.Country;
import com.cognizant.ormlearn.repository.CountryRepository;
import com.cognizant.ormlearn.service.exception.CountryNotFoundException;

@Service
public class CountryService {

	@Autowired
	CountryRepository countryRepository;
	@Transactional 
	public List<Country> getAllCountries() {
		 List<Country> cntr =  countryRepository.findAll();
		 return cntr;
	}
	
	@Transactional
	public Country findCountryByCode(String countryCode) throws CountryNotFoundException{
		 Optional<Country> result = countryRepository.findById(countryCode);
		 if (!result.isPresent()) {
			System.out.println("laillo");
		 }
		 Country country = result.get();
		 return country;
	}
	
	@Transactional
	public void addCountry(Country country) {
		
		countryRepository.save(country);
	}
     
	@Transactional
	public void deleteCountry(String id) {
		
		countryRepository.deleteById(id);
		
	}
	
	
	
	
	
}
