package com.cognizant.ormlearn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.cognizant.ormlearn.model.Country;
import com.cognizant.ormlearn.service.CountryService;
import com.cognizant.ormlearn.service.exception.CountryNotFoundException;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SpringBootApplication

public class OrmLearnApplication {
    private static CountryService countryService;
    private static final Logger LOGGER = LoggerFactory.getLogger(OrmLearnApplication.class);
    public static void main(String[] args) throws CountryNotFoundException {
		SpringApplication.run(OrmLearnApplication.class, args);
		
	    LOGGER.info("Inside main");
	    ApplicationContext context = SpringApplication.run(OrmLearnApplication.class, args);
       countryService = context.getBean(CountryService.class);

        testGetAllCountries();
        getAllCountriesTest();
        testAddCountry();
        delete();
	}
	
    private static void testGetAllCountries() {
        LOGGER.info("Start");
       List<Country> countries = countryService.getAllCountries();
        LOGGER.debug("countries={}", countries);
        LOGGER.info("End");
   }
    private static void getAllCountriesTest() throws CountryNotFoundException {
        LOGGER.info("Start");
        Country country = countryService.findCountryByCode("us");
        LOGGER.debug("Cgngh", country);
        LOGGER.info("End");
    }
    private static void testAddCountry() {
        LOGGER.info("Start");
        Country countr = new Country();
		countr.setCode("aa");
		countr.setName("sid");
       countryService.addCountry(countr);
       
        //LOGGER.debug("countries={}", countries);
        LOGGER.info("third");
   }
     
    private static void delete() {
        LOGGER.info("Startdelete");
       // Country countr = new Country();
		
       countryService.deleteCountry("us");
       
        //LOGGER.debug("countries={}", countries);
        LOGGER.info("delete");
   }
    
    


}
