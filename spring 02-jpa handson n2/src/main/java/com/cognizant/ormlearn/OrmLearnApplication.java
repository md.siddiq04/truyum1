package com.cognizant.ormlearn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.cognizant.ormlearn.model.Country;
import com.cognizant.ormlearn.model.Employee;
import com.cognizant.ormlearn.repository.DepartmentRepository;
import com.cognizant.ormlearn.repository.SkillRepository;
import com.cognizant.ormlearn.repository.StockRepository;
import com.cognizant.ormlearn.service.CountryService;
import com.cognizant.ormlearn.service.EmployeeService;
import com.cognizant.ormlearn.service.exception.CountryNotFoundException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SpringBootApplication

public class OrmLearnApplication {
    private static CountryService countryService;
    private static EmployeeService employeeService;
    private static  DepartmentRepository    depser;
    private static  SkillRepository skillser;
        private static final Logger LOGGER = LoggerFactory.getLogger(OrmLearnApplication.class);
    public static void main(String[] args) throws Exception {
		SpringApplication.run(OrmLearnApplication.class, args);
		
	    LOGGER.info("Inside main");
	    ApplicationContext context = SpringApplication.run(OrmLearnApplication.class, args);
	    employeeService = context.getBean(EmployeeService.class);
       //facebook();
       testGetEmployee();
       testEmployee();
     	}
	
       public static void facebook() {
    	countryService.stock();
    }
    

       private static void testGetEmployee() {
           LOGGER.info("Start");
           Employee employee = employeeService.get(1);
           LOGGER.debug("Employee:{}", employee);
           LOGGER.debug("Department:{}", employee.getDepartment());
           LOGGER.info("End");
       }
       private static void testEmployee() throws Exception {
           LOGGER.info("Save");
           Employee employee = new Employee();
           
           String sDate1="31/12/1998";  
          employee.setId(9);
           employee.setEm_dp_id(9);
           employee.setName("mdsiddiq");
           employee.setSalary(10500);
           employee.setPermanent(true);
           Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(sDate1);
           employee.setDateOfBirth(date1);
            employeeService.save(employee);;
                     LOGGER.info("End");
       }

       

}
