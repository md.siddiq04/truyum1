package com.cognizant.ormlearn.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="stock")
public class stock {
	
		  
	    @Id
	    private int ST_ID;
	    private String ST_CODE;
	    private Date ST_DATE;
	    private int ST_OPEN;
	    public Date getST_DATE() {
			return ST_DATE;
		}
		public void setST_DATE(Date sT_DATE) {
			ST_DATE = sT_DATE;
		}
		private int ST_CLOSE;
	    public int getST_ID() {
			return ST_ID;
		}
		public void setST_ID(int sT_ID) {
			ST_ID = sT_ID;
		}
		public String getST_CODE() {
			return ST_CODE;
		}
		public void setST_CODE(String sT_CODE) {
			ST_CODE = sT_CODE;
		}
	
		public int getST_OPEN() {
			return ST_OPEN;
		}
		public void setST_OPEN(int sT_OPEN) {
			ST_OPEN = sT_OPEN;
		}
		public int getST_CLOSE() {
			return ST_CLOSE;
		}
		public void setST_CLOSE(int sT_CLOSE) {
			ST_CLOSE = sT_CLOSE;
		}
		public int getST_VOLUME() {
			return ST_VOLUME;
		}
		public void setST_VOLUME(int sT_VOLUME) {
			ST_VOLUME = sT_VOLUME;
		}
		private int ST_VOLUME;
	   
}
