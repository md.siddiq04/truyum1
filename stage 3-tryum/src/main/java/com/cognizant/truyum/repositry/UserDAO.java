package com.cognizant.truyum.repositry;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.cognizant.truyum.model.User;

@Repository
public interface UserDAO extends JpaRepository<User, String> {

	@Query("FROM User WHERE username=:username and password=:password")
	public User getUserByUsernameAndPassword(@Param("username") String username, @Param("password") String password);
//

}
