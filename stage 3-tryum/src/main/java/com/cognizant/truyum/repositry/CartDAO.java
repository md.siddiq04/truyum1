package com.cognizant.truyum.repositry;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cognizant.truyum.model.Cart;

@Repository
public interface CartDAO extends JpaRepository<Cart, Integer> {

}
