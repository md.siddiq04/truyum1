package com.cognizant.truyum.repositry;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cognizant.truyum.model.Authorities;
@Repository
public interface AuthoritiesDAO extends JpaRepository<Authorities, Integer> {

}
