package com.cognizant.truyum;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.cognizant.truyum.model.Authorities;
import com.cognizant.truyum.model.Cart;
import com.cognizant.truyum.model.MenuItem;
import com.cognizant.truyum.model.User;
import com.cognizant.truyum.service.AuthoritiesService;
import com.cognizant.truyum.service.CartService;
import com.cognizant.truyum.service.MenuItemService;
import com.cognizant.truyum.service.UserService;

@SpringBootApplication
public class TryumApplication {
	private static final Logger LOGGER = LoggerFactory.getLogger(TryumApplication.class);
	public static UserService service;
	public static MenuItemService itemService;
	public static AuthoritiesService authoritiesService;
	public static CartService cartService;

	public static void main(String[] args) {

		ApplicationContext context = SpringApplication.run(TryumApplication.class, args);
		service = context.getBean(UserService.class);
		itemService = context.getBean(MenuItemService.class);
		authoritiesService = context.getBean(AuthoritiesService.class);
		cartService = context.getBean(CartService.class);
//		testapp();
//		testapp2();
//		testapp3();
//		testapp4();
		System.out.println("ThE END..............................................");
	}

	public static void testapp() {
		LOGGER.info("Start");
//		service.save(new User("abc", "abc", "prayas", "vijayvargiya", "prayasvij@gmail.com", 1));
//		LOGGER.debug("hello", service.getUserByUsernameAndPassword("prayas", "abc"));
		// System.out.println("done"+service.findByUsername("prayas").getAuthorities());
//		service.deleteByUserName("abc");
//		System.out.println("delete");
		LOGGER.info("End");
	}

	public static void testapp2() {
		LOGGER.info("Start");
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date date;
		try {
			date = formatter.parse("2020-04-02");
//			itemService.save(new MenuItem(0, "grapes", 80, false, date, "fruit", true, "imagegrapes.png"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
//		System.out.println(itemService.findById(6));
//		itemService.deleteById(7);
//		System.out.println(itemService.findAll());

		System.out.println(itemService.findByName("apple"));
		LOGGER.info("End");
	}

	public static void testapp3() {
		LOGGER.info("Start");

//		authoritiesService.save(new Authorities(2,"user",service.findByUsername("abc")));
//		System.out.println(authoritiesService.findAll());
//		System.out.println(authoritiesService.findById(8).getUser());
//		authoritiesService.deleteById(7);
//		System.out.println(authoritiesService.findAll());
		LOGGER.info("End");
	}

	public static void testapp4() {
		LOGGER.info("Start");
		User u = service.findByUsername("prayas");

		List<MenuItem> mlist = itemService.findAll();
		mlist.remove(1);
		
		cartService.save(new Cart(55, 1000, u, mlist));

//		System.out.println(itemService.findById(6));
//		itemService.deleteById(7);
//		System.out.println(cartService.findAll());

//		System.out.println(itemService.findByName("apple"));
		LOGGER.info("End");
	}

}
