package com.cognizant.truyum.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cognizant.truyum.model.Cart;

import com.cognizant.truyum.repositry.CartDAO;
import com.cognizant.truyum.service.Exception.CartException;

@Service
public class CartServiceImpl implements CartService {
	@Autowired
	CartDAO dao;

	@Transactional
	@Override
	public List<Cart> findAll() {
		// TODO Auto-generated method stub
		return (List<Cart>) dao.findAll();
	}

	@Transactional
	@Override
	public void save(Cart cart) {
		dao.save(cart);

	}

	@Transactional
	@Override
	public Cart findById(int id) {
		Optional<Cart> result = dao.findById(id);
		Cart user = null;
		if (result.isPresent()) {
			user = result.get();
		} else {
			throw new RuntimeException("user with id: " + id + " not found");
		}
		return user;

	}

	@Transactional
	@Override
	public void deleteById(int id) {
		dao.deleteById(id);

	}

	@Transactional
	@Override
	public void updateCart(int id, Cart cart) throws CartException {
		Optional<Cart> result = dao.findById(id);
		if (!result.isPresent()) {
			throw new CartException();
		}
		dao.save(cart);

	}

}
