package com.cognizant.truyum.service;

import java.util.List;

import com.cognizant.truyum.model.Cart;
import com.cognizant.truyum.service.Exception.CartException;

public interface CartService {
	public List<Cart> findAll();

	public void save(Cart cart);

	public Cart findById(int id);

	public void deleteById(int id);

	public void updateCart(int id, Cart cart) throws CartException;

}
