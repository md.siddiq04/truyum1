package com.cognizant.truyum.service;

import java.util.List;

import com.cognizant.truyum.model.User;

public interface UserService {

	public List<User> findAll();

//	public User findById(String Username);

	public void save(User user);

	public void deleteByUserName(String Username);

	public User getUserByUsernameAndPassword(String username, String password);

	public User findByUsername(String Username);

}
