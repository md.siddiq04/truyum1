package com.cognizant.MovieCruiser.Service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cognizant.MovieCruiser.DAO.MovieDAO;
import com.cognizant.MovieCruiser.Service.Exception.MovieException;
import com.cognizant.MovieCruiser.model.Movie;

@Service
public class MovieServiceImpl implements MovieService {
	@Autowired
	MovieDAO dao;

	@Transactional
	@Override
	public List<Movie> findAll() {

		return dao.findAll();
	}

	@Transactional
	@Override
	public Movie findById(int id) {
		// TODO Auto-generated method stub
		return dao.findById(id).get();
	}

	@Transactional
	@Override
	public List<Movie> findByTitle(String title) {
		return dao.findByTitle(title);

	}

	@Transactional
	@Override
	public void save(Movie movie) {
		dao.save(movie);

	}

	@Transactional
	@Override
	public void updateMovie(Movie movie) throws MovieException {
		Optional<Movie> result = dao.findById(movie.getId());
		if (!result.isPresent()) {
			throw new MovieException();
		}
		dao.save(movie);

	}

	@Transactional
	@Override
	public void deleteById(int id) {
		dao.deleteById(id);

	}

	@Transactional
	@Override
	public List<Movie> searchMovie(String find) {
		return dao.searchMovie(find);

	}

}
