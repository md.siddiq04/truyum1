package com.cognizant.MovieCruiser.Service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cognizant.MovieCruiser.DAO.Fav_movieDAO;
import com.cognizant.MovieCruiser.Service.Exception.FavMovieException;
import com.cognizant.MovieCruiser.model.Fav_movie;

@Service
public class FavMovieServiceImpl implements FavMovieService {
	@Autowired
	Fav_movieDAO dao;

	@Transactional
	@Override
	public List<Fav_movie> findAll() {
		return (List<Fav_movie>) dao.findAll();
	}

	@Transactional
	@Override
	public void save(Fav_movie favmovie) {
		dao.save(favmovie);

	}

	@Transactional
	@Override
	public Fav_movie findById(int id) {
		Optional<Fav_movie> result = dao.findById(id);
		Fav_movie user = null;
		if (result.isPresent()) {
			user = result.get();
		} else {
			throw new RuntimeException("user with id: " + id + " not found");
		}
		return user;
	}

	@Transactional
	@Override
	public void deleteById(int id) {
		dao.deleteById(id);
	}

	@Transactional
	@Override
	public void updateCart(int id, Fav_movie favmovie) throws FavMovieException {
		Optional<Fav_movie> result = dao.findById(id);
		if (!result.isPresent()) {
			throw new FavMovieException();
		}
		dao.save(favmovie);

	}

}
